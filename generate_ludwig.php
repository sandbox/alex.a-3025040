<?php
// SPDX-License-Identifier: GPL-2.0-only
// Copyright 2019 Alex Athanasopoulos

require_once("src/LudwigGenerator.php");

use Melato\Drupal\Install\LudwigGenerator;

try {
    $ludwig = new LudwigGenerator("composer.lock");

    $providers = array();
    $n = count($argv);
    for( $i = 1; $i < $n; $i++) {
        $providers[] = new LudwigGenerator($argv[$i]);
    }
    $ludwig->setProviders($providers);    
    $ludwig->writeLudwigFile("ludwig.json");
    $ludwig->writeTarScript("tar.sh");
} catch( Exception $e ) {
    printf("exception: %s\n", $e->getMessage());
}
