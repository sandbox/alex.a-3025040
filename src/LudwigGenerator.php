<?php
// SPDX-License-Identifier: GPL-2.0-only
// Copyright 2019 Alex Athanasopoulos

namespace Melato\Drupal\Install;
use Exception;

class Provider {
    public $name;
    public $version;
    public function __construct($name, $version) {
        $this->name = $name;
        $this->version = $version;
    }
}

class LudwigGenerator {
    protected $file;
    protected $name;
    protected $packages;
    protected $providers;
    protected $required;
    protected $provided;
    
    public function __construct(string $composerLockFile) {
        $this->file = $composerLockFile;
        $this->name = self::getPackageNameFromFile($this->file);
        $this->packages = self::readComposerLockPackages($composerLockFile);
        $this->required = $this->packages;
    }

    public static function getPackageNameFromFile(string $file) {
        $fields = array();
        $n = preg_match("#^.*/([^/]+)/[^/]+$#", $file, $fields);
        if ( $n == 1 ) {
            return $fields[1];
        } else {
            return $file;
        }
        
    }
    public function getName() {
        return $this->name;
        
    }
    
    private function split() {
        $required = array();
        $provided = array();
        foreach($this->packages as $name => $version) {
            $v = $this->getProvider($name);
            if ( ! $v ) {
                $required[$name] = $version;
            } else {
                $provided[$name] = $v;
            }
        }
        $this->required = $required;
        $this->provided = $provided;
    }
    
    public function print() {
        foreach($this->required as $name => $version) {
            printf("%s (%s)\n", $name, $version);
        }
        foreach($this->provided as $name => $p) {
            printf("%s (%s) provided version=%s by %s\n", $name, $this->packages[$name], $p->version, $p->name);
        }
    }
    
    public function setProviders($providers) {
        $this->providers = $providers;
        $this->split();
    }
    
    public static function readComposerLockPackages(string $composerLockFile): array {
        $json = self::readJson($composerLockFile);
        $packages = array();
        foreach($json->packages as $p) {
            $packages[$p->name] = $p->version;
        }
        return $packages;
    }
    public function getVersion(string $name) {
        foreach($this->packages as $p => $version) {
            if ( $p == $name ) {
                return $version;
            }
        }
        return NULL;
    }
    public function getProvider(string $name) {
        foreach($this->providers as $p) {
            $v = $p->getVersion($name);
            if ( $v ) {
                return new Provider($p->getName(), $v);
            }
        }
        return NULL;
    }
    public static function readJson(string $file) {
        $data = file_get_contents($file);
        if ( $data === FALSE ) {
            throw new Exception("cannot open " . $file);
        }
        $json = json_decode($data);
        if ( $json === FALSE ) {
            throw new Exception("invalid JSON: " . $file);
        }
        return $json;
    }
        
    public static function writeJson($obj, string $file) {
        $s = json_encode($obj, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);        
        if ( file_put_contents($file, $s) === FALSE ) {
            throw new Exception("cannot write: " . $file);            
        }
    }
    
    public function writeLudwigFile(string $ludwigFile) {
        $requireArray = array();
        foreach($this->required as $name => $version) {
            $requireArray[$name] = array( "version" => $version );
        }
        $json = array("require" => $requireArray);
        self::writeJson($json, $ludwigFile);          
    }
    
    public function writeTarScript(string $file) {
        $f = fopen($file, "w");
        if ( $f === FALSE ) {
            throw new Exception("cannot create " . $file);
        }
        try {
            fprintf($f, "#!/bin/sh\n");
            fprintf($f, "\n");
            fprintf($f, "mkdir -p lib\n");
            if ( $this->provided ) {
                fprintf($f, "mkdir -p lib/provided\n");                
            }
            fprintf($f, "\n");
            foreach($this->packages as $name => $version) {
                $uname = str_replace("/", "_", $name);
                $target = "lib";
                if ( array_key_exists($name, $this->provided) ) {
                    $target = $target . "/provided";
                }
                //tar cfz lib/psr_http-message-1.0.1.tar.gz -C vendor/psr/http-message --transform='s#^./#psr_http_message/1.0.1/#' ./
                fprintf($f, "tar cfz %s/%s-%s.tar.gz -C vendor/%s --transform='s#^./#%s/%s/#' ./\n", $target, $uname, $version, $name, $uname, $version);
            }
        } finally {
            fclose($f);
        }
    }    
}