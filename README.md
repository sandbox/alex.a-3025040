# Drupal Ludwig generator

This package generates ludwig.json files for Drupal 8 modules from the module composer.lock file.

It also packages the 3rd party libraries into individual .tar.gz files, which a system administrator can install manually at a Drupal Site with the Ludwig Module.

# To use it, you have to have access to a machine with composer.  Then transfer the resulting libraries and configuration files to a Drupal 8 site and install them without composer.
# Use as follows:

For Any Module that has a composer.json file with 3rd party dependencies:
- Download and unpack the module to your composer machine.
- cd <module-dir>
- composer install
- php <ludwig-generator-dir>/generate_ludwig.php <drupal-dir>/composer.lock

This will generate:
- ludwig.json
- tar.sh

run tar.sh to generate tar.gz files in ./lib/

On the Drupal site machine:
- Install the Ludwig module
- Extract the original module
- Copy ludwig.json to <module-dir>
- Create <module-dir>/lib
- Unpcack each of the generated .tar.gz files in <module-dir>/lib
- Clear Cache (may not be necessary)
- Install the module
